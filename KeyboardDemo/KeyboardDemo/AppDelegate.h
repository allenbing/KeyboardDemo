//
//  AppDelegate.h
//  KeyboardDemo
//
//  Created by yant on 16/2/15.
//  Copyright © 2016年 yant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

