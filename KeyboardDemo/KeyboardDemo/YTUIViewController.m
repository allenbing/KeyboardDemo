//
//  YTUIViewController.m
//  小依休
//
//  Created by 闫涛 on 15/12/2.
//  Copyright © 2015年 AnSaiJie. All rights reserved.
//

#import "YTUIViewController.h"
#import "Define.h"

@interface YTUIViewController ()
{
    CGFloat viewBottom;     //textField的底部
    CGFloat keyboardDuration;   //键盘隐藏需要的时间 = 键盘弹出的时间
}

@end




@implementation YTUIViewController


//移除监听
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self setupForKeyBoard];
}



#pragma mark -键盘相关
//键盘的准备
- (void)setupForKeyBoard
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    //点击手势点击非输入框区域隐藏键盘
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHideAction)];
    
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    
    
    
    //键盘显示后添加手势
    [nc addObserverForName:UIKeyboardWillShowNotification object:nil queue:mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        [self.view addGestureRecognizer:tap];
        [self keyboardWillShow:note];
    }];
    
    //键盘消失后移除手势
    [nc addObserverForName:UIKeyboardWillHideNotification object:nil queue:mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        [self.view removeGestureRecognizer:tap];
        
        //键盘动画时间
        double duration = [[note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        [UIView animateWithDuration:duration animations:^{
            self.view.frame = CGRectMake(0,  0 , SCREEN_WIDTH, SCREEN_HEIGHT);
        }];
    }];
}


//键盘消失后view下移
- (void)keyBoardHideAction
{
    NSLog(@"begin hide");
    [self.view endEditing:YES];
    
}


//通过note获取键盘高度，键盘动画时间
- (void)keyboardWillShow:(NSNotification *)note
{
    NSLog(@"后调用");
    
    //键盘高度
    CGFloat keyboardH = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    //键盘动画时间
    keyboardDuration = [[note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    if (viewBottom + keyboardH < SCREEN_HEIGHT) {
        return;
    }
    else
    {
        [UIView animateWithDuration:keyboardDuration animations:^{
            self.view.frame = CGRectMake(0, - ( keyboardH - (SCREEN_HEIGHT - viewBottom)), SCREEN_WIDTH, SCREEN_HEIGHT);
        }];
    }
}





#pragma mark -UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"最先调用");
    
    //将textField的rect转换到self.view上
    CGRect rect = [textField.superview convertRect:textField.frame toView:self.view];
    
    //textField的底部
    viewBottom = rect.origin.y + rect.size.height;

    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"最后调用");
    [UIView animateWithDuration:keyboardDuration animations:^{
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }];
    
}




@end
