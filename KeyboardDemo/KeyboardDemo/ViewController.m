//
//  ViewController.m
//  KeyboardDemo
//
//  Created by yant on 16/2/15.
//  Copyright © 2016年 yant. All rights reserved.
//

#import "ViewController.h"
#import "Define.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>


@property(nonatomic, strong)UITableView *tableView;

@property(nonatomic, strong)NSMutableArray *textFieldsArr;     //所有的输入框



@end

@implementation ViewController


- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:SCREEN_BOUNDS];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}


- (NSMutableArray *)textFieldsArr
{
    if (_textFieldsArr == nil) {
        _textFieldsArr = [[NSMutableArray alloc] init];
    }
    return _textFieldsArr;
}


#pragma mark 控制器
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    [self.view addSubview:self.tableView];
    
    //配置子控件
    [self setupSubViews];
}


- (void)setupSubViews
{
    CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH / 2, 40);
    
    for (int i = 0; i < 10; i++) {
        
        UITextField *textField = [[UITextField alloc] initWithFrame:rect];
        textField.placeholder = [NSString stringWithFormat:@"我是第%ui个输入框", i];
        textField.delegate = self;
        [self.textFieldsArr addObject:textField];
    }
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.textFieldsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifer = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"标题%d:",indexPath.row];
    cell.accessoryView = self.textFieldsArr[indexPath.row];
    
    return cell;
}





@end
